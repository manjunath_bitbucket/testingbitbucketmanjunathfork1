trigger Site_Duplicate_Check on Site__c (before insert, before update) {
 Set<string> StringSet = New Set<string>();
 for(Site__c site :trigger.new){
      StringSet.add(site.name);
    }
 Set<String> strSet = New Set<String>();
 for(Site__c siteList : [Select id,name from Site__c where name in:StringSet]){
          strSet.add(siteList.name);
     }
for(Site__c site :trigger.new){
         if(strSet.contains(site.name)){
            site.adderror('Already this site is existed, Please Create a New Site.');
           //trigger.new[0].name.addError('Already this site is existed, Please Create a New Site.');

          }
     }

}