/************************************
Name         : LCBS_BuildingAccessReqController
Created By   : Nathan Lourdusamy
Company Name : NTT Data
Project      : LCBS
Created Date : 
Test Class   : 
Test Class URL: 
Usages       : 
Modified By  :
***********************************/

public without sharing class LCBS_BuildingAccessReqController
{   
    //public Id BuldId {get; set;}
    public String BuldId {get;set;}
    //public String domainname {get; set;}
    public String siteurl {get;set;}
  
public string orgid {get;set;}
public document logo {get;set;}
public document sign {get;set;}
public string sfdcBaseURL{get;set;}
public String companyId {get;set;}

public Id buildingOwnerId{get;set;}
public string buildingOwnerName{get;set;}
public Building__c b{get;set;}
public String d {get;set;}
public Building_Owner__c Bo{get;set;}

 public Building__c fetchinfo {
       get {
          fetchinfo = [select id,createdBy.id,Building_Owner__r.id from Building__c where id ='a02g000000DQBiC'];
            system.debug('fetchinfo<<<<<<<<:'+fetchinfo);
            return fetchinfo;                        
        } 
        set;
        
    }   
   public LCBS_BuildingAccessReqController() 
    {
     //BuldId = ApexPages.CurrentPage().getparameters().get('id');
     // system.debug('BuldId<<<<<<<<:'+BuldId);
    
     //String dom = [SELECT Id, Domain FROM Domain].Domain;
     //List<String> envname = dom.split('-',2);     
     //String domainname = [SELECT Name, Env__c, Domain__c FROM LCBS_SiteDomains__c WHERE Name=:envname[0]].Domain__c;
     //siteurl = domainname+'/approverejectbuilding';
    // b = [select id,createdByid,Building_Owner__r.Name,Building_Owner__r.email_address__c from building__c where id =:build.id];
    // bo = [select id,name,email_address__c from Building_Owner__c where id=:b.Building_Owner__c];
     
     sfdcBaseURL = System.URL.getSalesforceBaseURL().toExternalForm();
     d= DateTime.now().format('MMMMM dd, yyyy');
    //system.debug(con);
    Site ste = [SELECT Id, Name, Description FROM Site WHERE Name='LCBS_BuildingApproveReject'];
     siteurl = ste.description;
     system.debug('site URL>>>>>>:'+siteurl);
  
  orgid= UserInfo.getOrganizationId();
  logo = [select id,name from document where name='LCBS_Email_Logo']; 
  sign = [select id,name from document where name='Honeywell vicepresident sign']; 
    }
public User u {
        get {
           u = [select id,name,AccountId from user where id=: build.createdByid]; 
      
           return u;                                                
        } 
        set;
    } 
    public Contact con {
        get {
          con = [select id,name,phone,Account.name,Account.Email__c from Contact where AccountId=:u.AccountId and Role__c='Owner' and LCBS_Program_Principal__c =true limit 1];
           return con;                                                
        } 
        set;
    }   
    
   public Building__c build {
        get {
            build = [select id,name,CreatedById,Building_Owner__r.Name,Building_Owner__r.email_address__c,Building_Owner__r.First_Name__c,Building_Owner__r.Last_Name__c from building__c where id =:BuldId];

            system.debug('fetchinfo<<<<<<<<:'+build);
            return build;                                                
        } 
        set;
    }  
}