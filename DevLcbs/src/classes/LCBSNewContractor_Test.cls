@isTest(SeeAllData=true)
public class LCBSNewContractor_Test
{
   static Building__c build;
   public LCBSNewContractor_Test(){ } 
    
  public static testMethod void myTest(){
      
      build = LCBS_TestDataFactory.createBuilding(1)[0];
       Account ac =new Account();
      ac.Name='ftest';
      ac.EnvironmentalContractorProNumber__c='2354';
      insert ac;
               
    Account a = new Account();
    a.Name='test';
    insert a;
    
    Contact con = new Contact();
      con.LastName ='test';
      RecordType RT = [select id,Name from RecordType where Name = 'ECC:Contractor Contacts'];
      con.RecordTypeId = RT.id;
      con.AccountId=a.id;
      con.Role__c = 'Primary Dispatcher';
      insert con;
      
  
    Profile p = [SELECT Id FROM Profile WHERE Name='Custom Customer Community Login User'];
    
    User u = new User(Alias = 'standt', Email='standarduser@testorg.com', 
    EmailEncodingKey='UTF-8', LastName='Testing', LanguageLocaleKey='en_US', 
    LocaleSidKey='en_US', ProfileId = p.Id, 
    TimeZoneSidKey='America/Los_Angeles', UserName='myuser@testorg.com',contactId = con.id);
    insert u;
    
      Opportunity o=new Opportunity();
      o.Name='test';
      o.CloseDate=Date.today();
      o.StageName='Prospecting';
      o.AccountId=a.id;
      o.OwnerId=u.id;
      insert o;
 
     
    Partner pa = new Partner();
      pa.OpportunityId=o.id; 
      pa.AccountToId = ac.id;
      pa.IsPrimary=true; 
      pa.Role='Advertiser';
      insert pa; 
      
      
     Company_Location__c loc = new Company_Location__c( name = 'Test Location' + Math.random(), 
      Address__c= 'test address',Country__c='United States',Email_Address__c='test@test.com',
       City__c = 'Testdale', State__c = 'ALABAMA', Postal_Code__c= '94803');
       insert loc;
      
     LCBSNewContractor c = new LCBSNewContractor();
      c.CPRONumber='1012';
     c.createContractor();
     c.getCompanyLoc();
     c.EmailContractor();
  //   c.addCPROContractor();

     Task newtask=new Task();
        newtask.Subject='Test Task'; 
        newtask.Description='Hello';
        newtask.Status='Not Started'; 
        newtask.Priority='High'; 
        newtask.ActivityDate=Date.Today()+1;
        newtask.Type='Other';
        insert newtask;        
        newtask.Type='OtherTest'; 
        update newtask;
      
        System.runAs(u){
       c.addCPROContractor();
          }
        
   
    System.runAs(u){
     PageReference pageRef = Page.LCBSContractorPage;
     pageRef.setRedirect(true);
      
     Profile p1 = [SELECT Id FROM Profile WHERE Name='Custom Customer Community Login User'];
     
     User user = [select ContactId from User where profileId=:p1.id and id = :userInfo.getUserId()];
     
     Contact con1 = [select LastName,AccountId ,Role__c from Contact where Id =:user.ContactId ];  
     
    PageReference pr = c.addCPROContractor();
    c.CPRONumber = 'invalid';
    pr = c.addCPROContractor();
      }
  }
      
 
}