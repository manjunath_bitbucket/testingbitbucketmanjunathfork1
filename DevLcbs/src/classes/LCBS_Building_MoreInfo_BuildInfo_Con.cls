public class LCBS_Building_MoreInfo_BuildInfo_Con{
public string buildingId {get;set;}
public Building__c b{get;set;}
public Building_Owner__c build {get;set;}
public Boolean buildingowner {get;set;}
public String PAGE_CSS { get; set; }
public String buildingOwnerStatus {get;set;}
Public boolean addBuildPanel {get;set;} 
Public boolean buildOwnerStatusPanel{get;set;} 
public user c {get;set;}
public user currentuser{get;set;}
public boolean isDistributor {
get {
User u = [select id,Contactid,AccountId,Account.EnvironmentalAccountType__c from User where id=:userinfo.getuserId()];
if(u.Account.EnvironmentalAccountType__c == 'Distributor')
          {
           isDistributor = true;
          }
          else if(u.Account.EnvironmentalAccountType__c == 'Contractor')
          {
           isDistributor = false;
          }
          return isDistributor;
}
set;}

  public LCBS_Building_MoreInfo_BuildInfo_Con(ApexPages.StandardController controller) {
      currentuser = [select id,name,contact.Role__c from user where id=:userinfo.getuserId()];
      buildingId = System.currentPageReference().getParameters().get('Id');
       Building__c b = [select id,name,CreatedBy.id,Owner_Approved__c,Owner_Rejected__c,OwnerId,Building_Owner__c from Building__c where id=:buildingId];
      c = [select id,Contact.Account.id from user where id=:b.Createdby.id];
      if(b.Building_Owner__c!=NULL)
      {
       buildingowner = true;
       buildOwnerStatusPanel = true;
       system.debug('chiran11**********');
       build = [select id,name,Address__c,City__c,Email_Address__c,Country__c,Postal_Code__c,Phone_Number__c,State__c from Building_Owner__c where id=:b.Building_Owner__c]; 
      }
      else
      {
      buildingowner = false;
      addBuildPanel = true;
      system.debug('chiran22**********');
      }
      system.debug('b.Owner_Approved__c+++'+b.Owner_Approved__c);
//      if(b.Owner_Approved__c==true){
//          AddBuildingCallout.geBuildings(b.name,string.valueof(b.Id));
//      }
 PAGE_CSS = LCBSParameters.PAGE_CSS_URL;
     if(b.Owner_Rejected__c == false && b.Owner_Approved__c==false)
     {
      buildingOwnerStatus = 'Invited';
     }
     else if(b.Owner_Rejected__c == false && b.Owner_Approved__c==true)
     {
      buildingOwnerStatus = 'Accepted';
     }
     else if(b.Owner_Rejected__c == true && b.Owner_Approved__c==false)
     {
      buildingOwnerStatus = 'Rejected';
     }
    }
public LCBS_Building_MoreInfo_BuildInfo_Con(){
 
 
  
  
} 
public pagereference equipmentPage()
{    
 //String equipment = 'tag:honeywell.com,2012:acs/system#_'+buildingId;
 //String equipment = buildingId;
 //Blob equipmentBlob = Blob.valueOf(equipment);
 //string convertedEquipment= EncodingUtil.base64Encode(equipmentBlob);
 PageReference p = new PageReference(LCBSParameters.Domain_URL+'/app/equipments/'+c.Contact.Account.id+'/'+buildingId);
 p.setRedirect(true);
 return p; 
}

public pagereference alertsPage()
{    
 //String equipment = 'tag:honeywell.com,2012:acs/system#_'+buildingId;
 //String equipment = buildingId;
 //Blob equipmentBlob = Blob.valueOf(equipment);
 // string convertedEquipment= EncodingUtil.base64Encode(equipmentBlob);
 PageReference p = new PageReference(LCBSParameters.Domain_URL+'/app/alerts/'+c.Contact.Account.id+'/'+buildingId);
 p.setRedirect(true);
 return p; 
}
public pagereference controlsPage()
{    
// String equipment = 'tag:honeywell.com,2012:acs/system#_'+buildingId;
 //String equipment = buildingId;
 //Blob equipmentBlob = Blob.valueOf(equipment);
 //string convertedEquipment= EncodingUtil.base64Encode(equipmentBlob);
 PageReference p = new PageReference(LCBSParameters.Domain_URL+'/app/controls/'+c.Contact.Account.id+'/'+buildingId);
 p.setRedirect(true);
 return p; 
}
public pageReference addBuildOwner()
{
  pageReference p = new  pageReference('/LCBSBuildingOwner?Id='+buildingId);
  p.setRedirect(true);
  return p;
}

}