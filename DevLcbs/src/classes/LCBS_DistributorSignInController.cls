/************************************
Name         : LCBS_DistributorSignInController
Created By   : Nagarajan Varadarajan
Company Name : NTT Data
Project      : LCBS
Created Date : May 5,2015
Test Class   : 
Test Class URL: 
Usages       : 
Modified By  : Chiranjeevi Gogulakonda
***********************************/
public Class LCBS_DistributorSignInController{
public String PAGE_CSS { get; set; }


public LCBS_DistributorSignInController()
{
   PAGE_CSS = LCBSParameters.PAGE_CSS_URL;
}
public PageReference forwardToStartPage() {
       // return Network.communitiesLanding();
         //string RecordTypeContractor = [SELECT id from RecordType where Name ='ECC:Contractor Contacts'].Id;
         //string RecordTypeDistributor = [SELECT id from RecordType where Name ='ECC:Distributor Contacts'].Id;
        User u = [Select id, Contact.Name,Contact.RecordTypeId,contact.AccountId,contact.Account.EnvironmentalAccountType__c from user where id=:userinfo.getUserId()];
        system.debug('######## Naga Test:Before Record Type IF');
        if(u.contact.Account.EnvironmentalAccountType__c == 'Distributor')
        {
           system.debug('######## Chest Test Account type:'+u.contact.Account.EnvironmentalAccountType__c);
           return Page.LCBSContractorPage;
        } 
        else if(u.contact.Account.EnvironmentalAccountType__c == 'Contractor')
        {
           system.debug('######## Chest Test Account type:'+u.contact.Account.EnvironmentalAccountType__c);
            /* String CommunityId = Network.getNetworkId();
            System.debug('CommunityId:::'+CommunityId);
            
            String CommunityUrl = Network.getLoginUrl(CommunityId);
            System.debug('CommunityUrl:::'+CommunityUrl);
            
            String Env = CommunityUrl.remove('https://').substringBefore('-');
            System.debug('Environment:::'+Env);
           
            LCBS_AzureUrls__c homepage = LCBS_AzureUrls__c.getValues(Env);
            
            LCBS_AzureUrls__c homepage = LCBS_AzureUrls__c.getValues(LCBSParameters.CURRENT_ENV);
            System.debug('homepage:::'+homepage);
        
           // return new PageReference(homepage.LCBS_HomeUrl__c); 
           pageReference p = new pageReference(homepage.LCBS_HomeUrl__c); */
           
           pageReference p = new pageReference(LCBSParameters.DISTRIBUTOR_HOME_URL);
           p.setRedirect(true);
           return p;
        } 
                 
        else
        {
         ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.ERROR, 'Login Failed or Your Account is not yet Approved. Please Try Again');
         ApexPages.addMessage(myMsg);
         //return Page.LCBS_Index;
         return null;
        }
    }

}