/************************************
Name         : LCBSBuildingOwnersController
Created By   : Chiranjeevi Gogulakonda
Company Name : NTT Data
Project      : LCBS
Created Date : 
Test Class   : 
Test Class URL: 
Usages       : 
Modified By  :
***********************************/

public class LCBSBuildingOwnersController {
    
    public boolean NewPanel { get; set; }  
    public Building_Owner__c BuldENew{set;get;}
    public boolean Ascending{get;set;}
    public boolean isDistributor {get;set;}
    public String PAGE_CSS { get; set; }


  // Contact cnt =  [SELECT ID, Firstname, Lastname, LastModifiedBy.Profile.Name FROM Contact where OwnerId=:UserInfo.getUserId() Limit 1 ];
    
 //  User user = [SELECT ID, ContactId, Contact.Firstname, Contact.Lastname, ProfileId, Profile.Name FROM User WHERE ContactID =:cnt.Id Limit 1];



    
    public List<Building_Owner__c> contactList {
        get {
            if (contactList == null) {
                contactList = [Select id,Name,Address_1__c,Address_2__c,City__c,Contractor_Branch_Location__c,Company_Company_Location12__c,
                Country__c,Email_Address__c,First_Name__c,Last_Name__c,Phone_Number__c,Postal_Code__c,State__c,Time_Zone__c from Building_Owner__c];
                system.debug(contactList);
            }
            return contactList;
        }
        set;
    }  
    public PageReference buildingOwnerInfo() {
       // return page.LCBSBuildingOwnerEdit;
        pagereference p = new pagereference('/apex/LCBSBuildingOwnerEdit'); 
        p.setredirect(true);
        return p;    
    } 
    public List<Building_Owner__c> Buld {get;set;}
    
    public pagereference Order(){
        if(Ascending==false){        
            Ascending = true;
           // try{           
               Buld = [Select id,Name,Address__c,Address_1__c,Address_2__c,City__c,Contractor_Branch_Location__c,
               Company_Company_Location12__c,Country__c,Email_Address__c,First_Name__c,Last_Name__c,
               Phone_Number__c,Postal_Code__c,State__c,Time_Zone__c from Building_Owner__c order by Name];    
         //   }Catch(NullPointerException npe){ ApexPages.addMessages(npe); }                 
        }else{
            Ascending = false;
    
                Buld = [Select id,Name,Address__c,Address_1__c,Address_2__c,City__c,Contractor_Branch_Location__c,
                Company_Company_Location12__c,Country__c,Email_Address__c,First_Name__c,Last_Name__c,Phone_Number__c,
                Postal_Code__c,State__c,Time_Zone__c from Building_Owner__c order by Name desc];  
           // }Catch(NullPointerException npe){ ApexPages.addMessages(npe); }     
        }
        return null;
    }    
    
    public LCBSBuildingOwnersController (){
        PAGE_CSS = LCBSParameters.PAGE_CSS_URL;
        NewPanel = true;
        Ascending=false; 
        BuldENew = new Building_Owner__c();   
        Buld = [Select id,Name,Address__c,Address_1__c,Address_2__c,City__c,Company_Company_Location12__c,Contractor_Branch_Location__c,Country__c,Email_Address__c,First_Name__c,Last_Name__c,Phone_Number__c,Postal_Code__c,State__c,Time_Zone__c from Building_Owner__c];              
         User u = [select id,Contactid,AccountId,Account.EnvironmentalAccountType__c from User where id=:userinfo.getuserId()];
         if(u.Account.EnvironmentalAccountType__c == 'Distributor')
          {
           isDistributor = true;
          }
          else if(u.Account.EnvironmentalAccountType__c == 'Contractor')
          {
           isDistributor = false;
          }
    }    
    public PageReference buidingCancel() {     
        pagereference p = new pagereference('/apex/LCBSBuildingOwners'); 
        p.setredirect(true);
        return p;     
    }  
    public PageReference buidingSave() {
        system.debug('Chiru Test:'+BuldENew); 
        try{        
            insert BuldENew;
        }
        catch(DmlException e){
            String error = e.getMessage();
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR,error));
            //ApexPages.addMessages(e);
        }        
        pagereference p = new pagereference('/apex/LCBSBuildingOwners'); 
        p.setredirect(true);
        return p;                                               
    }    
    public PageReference newBuildingOwner(){
        //return Page.LCBSNewBuildingOwner;        
        pagereference p = new pagereference('/apex/LCBSNewBuildingOwner'); 
        p.setredirect(true);
        return p;
    }  
    
    public SelectOption[] getContractorCompanyLoc() 
    {       
            SelectOption[] getContractorCompanyLocNames= new SelectOption[]{};  
                getContractorCompanyLocNames.add(new SelectOption('','--None--'));
            for (Company_Location__c co : [select id,Name from Company_Location__c order by name]) 
            {  
                getContractorCompanyLocNames.add(new SelectOption(co.id,co.Name));   
                
            }
            return getContractorCompanyLocNames;          
}
}