@isTest
public with sharing class LCBS_CompanyLocationTrigger_Test{
    public static testmethod void CompanyLocationTrigger(){
        Company_Location__c co = new Company_Location__c(Name='test');
        insert co;
        update co;
        delete co;
        undelete co;
    }
}