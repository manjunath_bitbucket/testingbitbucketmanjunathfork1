@isTest(seeAllData = true)
public class LCBS_EULADistributor_Test
{
   static Contact contact;
    static Account acc,acc1,acc2;
    
    static testMethod void myTest(){
        
        acc=LCBS_TestDataFactory.createAccounts(3)[0];
       
        Account a=new Account();
        a.Name ='test';
        a.EULA_Accepted__c = true;
        insert a;
        
        PageReference pageRef = Page.LCBSDistributorAgreement;
        Test.setCurrentPageReference(pageRef);
        ApexPages.currentPage().getParameters().put('id', a.id);
        Id currentRecordId =a.id;    
        
        LCBS_EULADistributor_Controller ed = new LCBS_EULADistributor_Controller();
        boolean rej = ed.Rejected;
        ed.Approved=True;
        ed.approveAction();
        
        List<Apexpages.Message> pageMessages = ApexPages.getMessages();
        
        ed.rejectAction();
        
        }
    static testmethod void testapproveflag() {
    
       acc1=LCBS_TestDataFactory.createAccounts(3)[0];
        
        Account a=new Account();
        a.Name ='test';
        a.EULA_Accepted__c = true;
        insert a;
        
       PageReference PageRef2 = Page.LCBSDistributorAgreement;
        Test.setCurrentPageReference(pageRef2);
        ApexPages.currentPage().getParameters().put('id', a.id);
        Id currentRecordId =a.id;
        
        LCBS_EULADistributor_Controller edc = new LCBS_EULADistributor_Controller ();
        edc.fetchaccinfo.EULA_Accepted__c = true;
        boolean app = edc.Approved;
        edc.fetchaccinfo.EULA_Accepted__c = false;
        boolean app1 = edc.Approved; 
    }
    
    static testmethod void testcatchblock() {
        
        acc2=LCBS_TestDataFactory.createAccounts(3)[0];
        
        Account a=new Account();
        a.Name ='test';
        a.EULA_Accepted__c = true;
        insert a;
         
        PageReference PageRef3 = Page.LCBSDistributorAgreement;
        Test.setCurrentPageReference(pageRef3);
        ApexPages.currentPage().getParameters().put('id', a.id);
        Id currentRecordId =a.id;
        
        LCBS_EULADistributor_Controller ed1 = new LCBS_EULADistributor_Controller ();

        delete ed1.fetchaccinfo;
        ed1.approveAction(); 
        
    } 
}