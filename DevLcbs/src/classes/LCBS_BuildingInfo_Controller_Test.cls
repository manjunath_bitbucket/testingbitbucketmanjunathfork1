@isTest(SeeAllData=true)
public class LCBS_BuildingInfo_Controller_Test 
{

    static Contact contact;
    static Building__c build;
    static Building_Owner__c buildowner;
    static EmailTemplate e;
    static testMethod void myTest(){
        e = new EmailTemplate (developerName = 'test', FolderId = '00Dg0000003Nq6jEAC', TemplateType= 'Text', Name = 'test'); 
        if (Test.isRunningTest()) {  
          System.runAs(new User(Id = Userinfo.getUserId())) {
          insert e;
       }
      } else {   
        insert e;
     }
        build = LCBS_TestDataFactory.createBuilding(1)[0];      
        contact=LCBS_TestDataFactory.createContactDummy(3)[0];
        buildowner = LCBS_TestDataFactory.createBuildingOwner(1)[0];        
        Task newtask=new Task();
        newtask.Subject='Test Task'; 
        newtask.Description='Hello';
        newtask.Status='Not Started'; 
        newtask.Priority='High'; 
        newtask.ActivityDate=Date.Today()+1;
        newtask.Type='Other';
        insert newtask;        
        newtask.Type='OtherTest'; 
        update newtask;
         
        PageReference pageRef = Page.LCBS_BuildingInformation;
        Test.setCurrentPageReference(pageRef);
        ApexPages.currentPage().getParameters().put('id', build.id); 
        
        PageReference pageRef1 = Page.LCBS_AllBuildings;
        Test.setCurrentPageReference(pageRef1); 
        
        LCBS_BuildingInfo_Controller bc = new LCBS_BuildingInfo_Controller();
        bc.getContractorCompanyLocOptions();
        bc.getContractorCompanyLocation();
        bc.getPrimaryDispatcherOptions();
        bc.getPrimaryTechOptions();
        bc.getSecondaryTechOptions();
        bc.getSeconDispatcherOptions();
        bc.savePopClose();
        bc.cancelPop();     
        bc.save();
        bc.cancel();
        bc.closePopup();
        bc.showPopup();
       
        List<SelectOption> testOptnssdispatcher = bc.getSeconDispatcherOptions();
        List<SelectOption> testOptnsComLoc = bc.getContractorCompanyLocOptions();
        List<SelectOption> testOptnsbuildowner = bc.getContractorCompanyLocation();
        List<SelectOption> testOptnspdispatcher = bc.getPrimaryDispatcherOptions();
        List<SelectOption> testOptnsptech = bc.getPrimaryTechOptions();
        List<SelectOption> testOptnsstech = bc.getSecondaryTechOptions();
       
    }
    static testMethod void myTest2(){
        build = LCBS_TestDataFactory.createBuilding(1)[0];
        contact=LCBS_TestDataFactory.createContactDummy(3)[0];
        buildowner = LCBS_TestDataFactory.createBuildingOwner(1)[0];
    
    LCBS_BuildingInfo_Controller bc = new LCBS_BuildingInfo_Controller();
    
    bc.createNewSite = true;

     
     Contact con = new Contact ();
     con.LastName='Test';
     con.Phone= '(415) 419-8873';
     con.Email = 'test@nttdata.com';
     con.role__c ='Secondary Technician';
     insert con;
     //bc.buildinfo = buildowner;
     bc.getSecondaryTechOptions();
    // bc.getContactDetails();
     delete con;
     bc.getSecondaryTechOptions();
     //bc.fetchinfo = build;
     //bc.buildinfo = buildowner;
     bc.fetchinfo.Building_Owner__c = buildowner.id;
     bc.save();
     
     EmailTemplate myEMailTemplate = [Select id from EmailTemplate where Name =: 'LCBS_BuildingAccessReqTemplate'];                 
}
   
   
}